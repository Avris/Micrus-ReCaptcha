<?php
namespace Avris\Micrus\ReCaptcha\Widget;

use Avris\Forms\Widget\DontMapOnObject;
use Avris\Forms\Widget\Widget;
use Avris\Micrus\ReCaptcha\Assert\ReCaptcha as ReCaptchaAssert;

final class ReCaptcha extends Widget implements DontMapOnObject
{
    /** @var string */
    private $siteKey;

    /** @var string */
    private $secretKey;

    public function __construct(string $envRecaptchaSiteKey, string $envRecaptchaSecretKey)
    {
        parent::__construct();
        $this->siteKey = $envRecaptchaSiteKey;
        $this->secretKey = $envRecaptchaSecretKey;
    }

    public function setOptions(array $options)
    {
        return parent::setOptions($options + ['label' => '']);
    }

    public function getSiteKey(): string
    {
        return $this->siteKey;
    }

    protected function getDefaultAssert()
    {
        return new ReCaptchaAssert($this->secretKey);
    }
}

