<?php
namespace Avris\Micrus\ReCaptcha\Assert;

use Avris\Forms\Assert\Assert;
use Avris\Forms\Assert\IsRequired;

final class ReCaptcha extends Assert implements IsRequired
{
    /** @var string */
    private $secretKey;

    public function __construct(string $secret, $message = null)
    {
        $this->secretKey = $secret;
        parent::__construct($message);
    }

    public function validate($value): bool
    {
        $recaptcha = new \ReCaptcha\ReCaptcha($this->secretKey);

        return $recaptcha->verify($_POST['g-recaptcha-response'])->isSuccess();
    }
}
