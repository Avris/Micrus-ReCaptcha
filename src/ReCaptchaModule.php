<?php
namespace Avris\Micrus\ReCaptcha;

use Avris\Micrus\Bootstrap\ModuleInterface;
use Avris\Micrus\Bootstrap\ModuleTrait;
use Avris\Micrus\Tool\Config\ParametersProvider;

class ReCaptchaModule implements ModuleInterface, ParametersProvider
{
    use ModuleTrait;

    public function getParametersDefaults(): array
    {
        return [
            'RECAPTCHA_SITE_KEY' => '',
            'RECAPTCHA_SECRET_KEY' => '',
        ];
    }
}
