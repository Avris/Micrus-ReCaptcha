# Micrus ReCaptcha Plugin ##

This is a module for [Micrus framework](http://micrus.avris.it)
that adds Google's [ReCaptcha](https://www.google.com/recaptcha) as a form widget.

## Installation

Run:

    composer require avris/micrus-recaptcha

Then register the module in your `App\App:registerModules`: 

    yield new \Avris\Micrus\ReCaptcha\ReCaptchaModule;

Finally, run:

    bin/env
    
You will be asked for your `Site key` and `Secret key`.
You can get them after signing up at [ReCaptcha](https://www.google.com/recaptcha).
They will be saved in the `.env` file.

## Usage
    
In your form's `configure()` method simply add this widget:

    ->add('captcha', \Avris\Micrus\ReCaptcha\Widget\ReCaptcha::class)
    
Voilà!

## Copyright

* **Author:** Andre Prusinowski [(Avris.it)](https://avris.it)
* **Licence:** [MIT](https://mit.avris.it)
